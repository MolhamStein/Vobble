//
//  Angle.swift
//  Vobble
//
//  Created by Abd Hayek on 12/5/19.
//  Copyright © 2019 Brain-Socket. All rights reserved.
//

import Foundation

postfix operator °

protocol IntegerInitializable: ExpressibleByIntegerLiteral {
    init (_: Int)
}

extension Int: IntegerInitializable {
    postfix public static func °(lhs: Int) -> CGFloat {
        return CGFloat(lhs) * .pi / 180
    }
}

extension CGFloat: IntegerInitializable {
    postfix public static func °(lhs: CGFloat) -> CGFloat {
        return lhs * .pi / 180
    }
}
