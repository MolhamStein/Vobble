//
//  ChatTutorialViewController.swift
//  twigbig
//
//  Created by Molham Mahmoud on 2017/05/01.
//  Copyright (c) 2017 Brain-socket. All rights reserved.
//

import UIKit
import Gecco

class ChatTutorialViewController: SpotlightViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var annotationViews: [UIView]!
    
    @IBOutlet var lblStep1: UILabel!
    @IBOutlet var lblStep2: UILabel!
    @IBOutlet var imgStep2: UIImageView!
    
    @IBOutlet var floatingAnimationViewStep2: JRMFloatingAnimationView?
    
    var stepIndex: Int = 0
    var showStep2: Bool = true
    var jumpToStep2: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        delegate = self
        if jumpToStep2 {
            stepIndex = 1
        }
        
//        // make tutorial repond to pan too
//        let panRecognizer = UIPanGestureRecognizer.init(target: self, action: #selector(handlePan))
//        panRecognizer.delegate = self
//        self.view.addGestureRecognizer(panRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func next(_ labelAnimated: Bool) {
        updateAnnotationView(labelAnimated)
        
        switch stepIndex {
        case 0:
            let xPos = (AppConfig.currentLanguage == .arabic) ? 100 : UIScreen.main.bounds.width - 100
            //spotlightView.appear(Spotlight.Oval(frame: CGPoint(x: xPos, y: 100, diameter: 100)))
            spotlightView.appear(Spotlight.Oval(center: CGPoint(x: xPos, y: 45), diameter: 110))
        case 1:
            if showStep2 {
                let xPos = (AppConfig.currentLanguage == .arabic) ? 35 : UIScreen.main.bounds.width - 35
                //spotlightView.appear(Spotlight.Oval(frame: CGPoint(x: xPos, y: 100, diameter: 100)))
                spotlightView.appear(Spotlight.Oval(center: CGPoint(x: xPos, y: 50), diameter: 70))
            }else {
                dismiss(animated: true, completion: nil)
            }
        case 2:
            dismiss(animated: true, completion: nil)
            //spotlightView.appear(Spotlight.Oval(center: CGPoint(x: screenSize.width / 2, y: screenSize.height/2), diameter: 0))
        default:
            break
        }
        
        stepIndex += 1
    }
    
    func updateAnnotationView(_ animated: Bool) {
        
        UIView.animate(withDuration: animated ? 0.4 : 0) {
            switch self.stepIndex {
            case 0:
                self.lblStep1.alpha = 1
                self.lblStep2.alpha = 0
                self.imgStep2.alpha = 0
                self.lblStep1.text = "TUT_CHAT_1".localized
                self.lblStep1.font = AppFonts.xBigBold
            case 1:
                self.lblStep1.alpha = 0
                self.lblStep2.alpha = 1
                self.imgStep2.alpha = 1
                self.lblStep2.text = "TUT_CHAT_2".localized
                self.lblStep2.font = AppFonts.xBigBold
                
                self.floatingAnimationViewStep2 = JRMFloatingAnimationView.init(starting: CGPoint(x:self.imgStep2.center.x + 55, y:self.imgStep2.center.y + 5)) ;
                self.floatingAnimationViewStep2?.fadeOut = true;
                self.floatingAnimationViewStep2?.varyAlpha = false;
                self.floatingAnimationViewStep2?.removeOnCompletion = false
                self.floatingAnimationViewStep2?.add(UIImage(named:"call"))
                self.floatingAnimationViewStep2?.add(UIImage(named:"call"))
                self.floatingAnimationViewStep2?.add(UIImage(named:"call"))
                self.floatingAnimationViewStep2?.floatingShape = .curveRight;
                self.floatingAnimationViewStep2?.maxFloatObjectSize = 40
                self.view.addSubview(self.floatingAnimationViewStep2!)
                self.floatingAnimationViewStep2?.animate()
                self.floatingAnimationViewStep2?.animate()
                self.floatingAnimationViewStep2?.animate()
                self.floatingAnimationViewStep2?.animate()
                dispatch_main_after(1) {
                    self.floatingAnimationViewStep2?.animate()
                    self.floatingAnimationViewStep2?.animate()
                    self.floatingAnimationViewStep2?.animate()
                }
                self.floatingAnimationViewStep2?.imageViewAnimationCompleted = { (img) -> Void in
                       self.floatingAnimationViewStep2?.animate()
                    };
            case 2:
                // this is an empty step
                // closing the first tutorial
                self.lblStep1.alpha = 0
                self.lblStep2.alpha = 0
            case 3:
                self.lblStep1.alpha = 0
                self.lblStep2.alpha = 0
            case 4:
                self.lblStep1.alpha = 0
                self.lblStep2.alpha = 0
            default:
                break
            }
        }
    }
    
    @IBAction func actionClose(_ sender: AnyObject){
        dismiss(animated: true, completion: nil)
        onCLose()
    }
    
    func onCLose() {
        self.floatingAnimationViewStep2?.imageViewAnimationCompleted = nil
        self.floatingAnimationViewStep2 = nil
    }
}

extension ChatTutorialViewController: SpotlightViewControllerDelegate {
    func spotlightViewControllerWillPresent(_ viewController: SpotlightViewController, animated: Bool) {
        next(false)
    }
    
    func spotlightViewControllerTapped(_ viewController: SpotlightViewController, isInsideSpotlight: Bool) {
        next(true)
    }
    
    func spotlightViewControllerWillDismiss(_ viewController: SpotlightViewController, animated: Bool) {
        spotlightView.disappear()
        onCLose()
    }
}

