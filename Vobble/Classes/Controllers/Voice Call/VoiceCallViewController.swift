//
//  VoiceCallViewController.swift
//  Vobble
//
//  Created by Abd Hayek on 12/25/19.
//  Copyright © 2019 Brain-Socket. All rights reserved.
//

import UIKit
import AgoraRtcKit
import Firebase
import Flurry_iOS_SDK
import SwiftyJSON

class VoiceCallViewController: AbstractController {
    
    @IBOutlet weak var muteAudioButton: UIButton!
    @IBOutlet weak var endCallButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var lblCallStatus: UILabel!
    @IBOutlet weak var imgUserPhoto: UIImageView!
    @IBOutlet weak var imgBackgroundUserPhoto: UIImageView!
    
    public var rtcEngine: AgoraRtcEngineKit?
    public var session: String = ""
    public var channelId = ""
    public var user: AppUser?
    public var isCalling: Bool = false
    public var log : CallLog?
    public var voipSnapshot: DatabaseReference?
    
    fileprivate var isAudioMuted = false {
        didSet {
            muteAudioButton?.setImage(isAudioMuted ? #imageLiteral(resourceName: "mute-microphone") : #imageLiteral(resourceName: "mic"), for: .normal)
        }
    }
    //fileprivate var audioOutputRouting: AgoraAudioOutputRouting?
    fileprivate var isSpeakerEnabled: Bool = true {
        didSet {
            rtcEngine?.setEnableSpeakerphone(isSpeakerEnabled)
            speakerButton?.backgroundColor = isSpeakerEnabled ? #colorLiteral(red: 0.2986887991, green: 0.6970970631, blue: 1, alpha: 1) : UIColor.clear
        }
    }
    fileprivate var isCallActive = false {
        didSet {
            isAudioMuted = false
        }
    }
    
    fileprivate var remoteUid: UInt = 0
    fileprivate var timer : Timer?
    fileprivate var timeCounter: Int = 0
    fileprivate var secondsCounter: Int = 0
    fileprivate var uid: UInt = 0
    
    fileprivate var ringingSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "Phone_Ringing", ofType: "mp3")!)
    fileprivate var busySound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "busy_signal", ofType: "mp3")!)
    fileprivate var insuficientCoinsSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "insuficient_cions", ofType: "mp3")!)
    fileprivate var busyPlayer : AVAudioPlayer?
    fileprivate var ringingPlayer : AVAudioPlayer?
    fileprivate var insuficientPlayer : AVAudioPlayer?
    
    var currentCallStatus: CallStatus?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        CallCenter.shared.delegate = self
        rtcEngine = AgoraRtcEngineKit.sharedEngine(withAppId: AppConfig.agoraAppId, delegate: self)

        self.fillUpRemoteUserData()
        self.startSession()
        self.lblTimer.text = "Connecting.."
        self.isSpeakerEnabled = true
        
        AppConfig.isAnotherCall = true
        
        if isCalling {
            Flurry.logEvent(AppConfig.call_calling)
            //fatch call status
            let conversationRef = FirebaseManager.shared.voipRef.child(voipSnapshot?.key ?? "")
            conversationRef.observeSingleEvent(of: .value, with: { [weak self] snapshot in
                print(snapshot)
                self?.onCallStatusChanged(snapshot: snapshot)
            })
        
            // status observer
            CallCenter.shared.channelKey = voipSnapshot?.key ?? ""
            FirebaseManager.shared.voipRef.child(voipSnapshot?.key ?? "").observe(.childChanged, with: {snapshot in
                self.onCallStatusChanged(snapshot: snapshot)
                
            }){ (err) in
                print(err.localizedDescription)
            }
        }
    }
    
    private func onCallStatusChanged (snapshot: DataSnapshot) {
        if snapshot.key == "status" {
            let status = CallStatus(rawValue: snapshot.value as! String)
            self.currentCallStatus = status
            
            if let status = status {
                switch status {
                    
                case .connecting:
                    self.lblTimer.text = status.getString()
                case .busy:
                    self.lblTimer.text = status.getString()
                    
                    self.stopRingingSound()
                    self.playBusySound()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2 ){
                        
                        self.doEndCallPressed(self.endCallButton)
                    }
                case .decline:
                    self.lblTimer.text = status.getString()
                    
                    self.stopRingingSound()
                    self.playBusySound()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        
                        self.doEndCallPressed(self.endCallButton)
                    }
                case .ringing:
                    self.lblTimer.text = status.getString()
                    
                    self.playRingingSound()
                case .missedCall:
                    print("missed call")
                    
                case .notReceivingCalls:
                    self.lblTimer.text = status.getString()
                    
                    self.stopRingingSound()
                    self.playBusySound()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2){
                        
                        self.doEndCallPressed(self.endCallButton)
                    }

                case .answering:
                    self.lblTimer.text = status.getString()
                }
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        stopTimer()
        AppConfig.isAnotherCall = false
    }

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        super.dismiss(animated: flag, completion: completion)
        FirebaseManager.shared.voipRef.removeAllObservers()
    }
    
    @IBAction func doMutePressed(_ sender: UIButton) {
        self.muteAudio(!isAudioMuted)
        //CallCenter.shared.muteAudio(of: session, muted: !isAudioMuted)
    }
    
    @IBAction func doEndCallPressed(_ sender: UIButton) {
        
        if let status = self.currentCallStatus, let peer = user {
            if ( (status == .connecting) || (status == .ringing) ) {
                // send a push notification for missed call
                let msgToSend = String(format: "NOTIFICATION_MISSED_CALL".localized, (DataStore.shared.me?.userName ?? ""))
                let msgToSendAr = String(format: "NOTIFICATION_MISSED_CALL_AR".localized, (DataStore.shared.me?.userName ?? ""))
                ApiManager.shared.sendPushNotification(msg: msgToSend, msg_ar: msgToSendAr, targetUser: peer, chatId: nil, completionBlock: { (success, error) in })
                
                // Observe sendMissedCall function in ChatViewController
                NotificationCenter.default.post(name: Notification.Name("MissedCall"), object: nil)
            }
        }
        
        CallCenter.shared.endCall(of: session)
        FirebaseManager.shared.changeVoipStatus(status: .decline, key: CallCenter.shared.channelKey)
        self.stopSession()
        self.stopRingingSound()
        self.stopBusySound()
        
        if isCalling {
            self.updateLog(true, didAnswer: false)
        }
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doSpeakerPressed(_ sender: UIButton) {
        isSpeakerEnabled = !isSpeakerEnabled
    }
}

extension VoiceCallViewController {
    func startSession() {
//        do {
//            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)

//        }catch {}
        
        isCallActive = true
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playAndRecord,
                                                         mode: AVAudioSession.Mode(rawValue: convertFromAVAudioSessionMode(AVAudioSession.Mode.voiceChat)),
                                                         options: [.mixWithOthers, .allowBluetooth])
        try? AVAudioSession.sharedInstance().setActive(true)
        
        //rtcEngine?.setAudioSessionOperationRestriction(.all)
        //rtcEngine?.setParameters("{\"che.audio.use.callkit\":true}")
        
        rtcEngine?.startPreview()
        rtcEngine?.setDefaultAudioRouteToSpeakerphone(true)
        
        rtcEngine?.joinChannel(byToken: nil, channelId: self.channelId, info: nil, uid: 0, joinSuccess: {_, uid, _ in
            
            self.uid = uid
            self.enableMedia(true)
            UIApplication.shared.isIdleTimerDisabled = true
        })
    }
    
    func enableMedia(_ enable: Bool) {
//        if enable {
//            rtcEngine?.enableAudio()
//            
//        } else {
//            rtcEngine?.disableAudio()
//
//        }
    }   
    
    func muteAudio(_ mute: Bool) {
        isAudioMuted = mute
        rtcEngine?.muteLocalAudioStream(mute)
    }
    
    func stopSession() {
        isCallActive = false
        //audioOutputRouting = nil
        
        rtcEngine?.leaveChannel({ (AgoraChannelStats) in
            print("leave result")
        })
        rtcEngine?.stopPreview()
        
        UIApplication.shared.isIdleTimerDisabled = false
        
        stopTimer()
        self.dismiss(animated: true, completion: nil)
    }
    
    func fillUpRemoteUserData(){
        lblUsername.text = session
        if let imgUrl = user?.profilePic, imgUrl.isValidLink() {
            imgUserPhoto.sd_setShowActivityIndicatorView(true)
            imgUserPhoto.sd_setIndicatorStyle(.gray)
            imgUserPhoto.sd_setImage(with: URL(string: imgUrl))
            imgBackgroundUserPhoto.sd_setShowActivityIndicatorView(true)
            imgBackgroundUserPhoto.sd_setIndicatorStyle(.gray)
            imgBackgroundUserPhoto.sd_setImage(with: URL(string: imgUrl))
        }else {
            imgUserPhoto.image = UIImage(named:"user_placeholder")
            imgBackgroundUserPhoto.image = UIImage(named:"user_placeholder")
        }
    }
    
    func setTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
            timer?.fire()
        }
        
    }
    
    func stopTimer(){
        timer?.invalidate()
        timeCounter = 0
        secondsCounter = 0
    }
    
    @objc
    func updateTimer(){
        self.timeCounter += 1
        self.secondsCounter += 1
        self.lblTimer.text = timeFormatted(totalSeconds: self.timeCounter)
        
        if secondsCounter == 60 {
            self.secondsCounter = 0
            
            if isCalling {
                if (DataStore.shared.me?.pocketCoins ?? 0) >= (DataStore.shared.me?.audioCallCost ?? 0) {
                    
                    self.updateLog(false, didAnswer: false)
                }else {
                    
                    self.showAlert(title: "Warning", message: "INSUFICIENT_COINS_MSG".localized, actions: nil)
                    self.doEndCallPressed(self.endCallButton)
                }
                
                
            }
        }
    }

    func timeFormatted(totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d:%02d", hours, minutes, seconds)
    }
    
    func playRingingSound () {
        // play beep sound
        do {
            // Prepare beep player
            self.ringingPlayer = try AVAudioPlayer(contentsOf: ringingSound as URL)
        
            self.ringingPlayer?.prepareToPlay()
            self.ringingPlayer?.numberOfLoops = 10
            self.ringingPlayer?.play()
            
        }catch {}
    }
    
    func stopRingingSound(){
        if let _ = self.ringingPlayer {
            self.ringingPlayer?.stop()
            self.ringingPlayer  = nil
        }
    }
    
    func playBusySound () {
        // play beep sound
        do {
            // Prepare beep player
            self.busyPlayer = try AVAudioPlayer(contentsOf: busySound as URL)
            
            self.busyPlayer?.prepareToPlay()
            self.busyPlayer?.numberOfLoops = 10
            self.busyPlayer?.play()
            
        }catch {}
    }
    
    func stopBusySound(){
        if let _ = self.busyPlayer {
            self.busyPlayer?.stop()
            self.busyPlayer  = nil
        }
    }
    
    func playInsuficientCoinsSound () {
        // play beep sound
        do {
            // Prepare beep player
            self.insuficientPlayer = try AVAudioPlayer(contentsOf: insuficientCoinsSound as URL)
            
            self.insuficientPlayer?.prepareToPlay()
            self.insuficientPlayer?.numberOfLoops = 1
            self.insuficientPlayer?.play()
            
        }catch {}
    }
    
    func updateLog(_ isFinish: Bool = false, didAnswer: Bool = false) {
        let cost = DataStore.shared.me?.audioCallCost ?? 0
        let tempLog = self.log
        let isReview = DataStore.shared.me?.version?.status == .inReview ? true : false
        
        tempLog?.status = "connecting"
        tempLog?.endAt = DateHelper.getISOStringFromDate(Date())
        tempLog?.isFinish = isFinish
        
        // For first update
        if didAnswer {
            tempLog?.startAt = DateHelper.getISOStringFromDate(Date())
        }
        
        if !isReview {
            DataStore.shared.me?.pocketCoins = (DataStore.shared.me?.pocketCoins ?? 0) - cost
            
            if let coins = DataStore.shared.me?.pocketCoins, coins <= cost, !isFinish {
                playInsuficientCoinsSound()
            
                // shoe alert to buy more coins
                let alertController = UIAlertController(title: "", message: "NO_ENOUGH_COINS_MSG".localized, preferredStyle: .alert)
                let ok = UIAlertAction(title: "GET_COINS".localized, style: .default, handler: { (alertAction) in
                    let shopVC = UIStoryboard.mainStoryboard.instantiateViewController(withIdentifier: ShopViewController.className) as! ShopViewController
                    shopVC.fType = .coinsPack
                    
                    self.present(shopVC, animated: true, completion: nil)
                })
            
                let cancel = UIAlertAction(title: "Cancel".localized, style: .default,  handler: nil)
                alertController.addAction(cancel)
                alertController.addAction(ok)
            
                self.present(alertController, animated: true, completion: nil)
            }
        }
        
        ApiManager.shared.updateCallLog(log: tempLog!, isReview: isReview, completionBlock: {sucess, error, log in
            
        })
    }
}

extension VoiceCallViewController: CallCenterDelegate {
    func callCenter(_ callCenter: CallCenter, startCall session: String) {
        enableMedia(false)
        startSession()
    }
    
    func callCenter(_ callCenter: CallCenter, answerCall session: String) {
        //enableMedia(true)
        //startSession()
        setTimer()
        callCenter.setCallConnected(of: session)
    }
    
    func callCenter(_ callCenter: CallCenter, declineCall session: String) {
        print("call declined")
        stopSession()
    }
    
    func callCenter(_ callCenter: CallCenter, muteCall muted: Bool, session: String) {
        muteAudio(muted)
    }
    
    func callCenter(_ callCenter: CallCenter, endCall session: String) {
        stopSession()
    }
    
    func callCenterDidActiveAudioSession(_ callCenter: CallCenter) {
        enableMedia(true)
    }
}

extension VoiceCallViewController: AgoraRtcEngineDelegate {
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinedOfUid uid: UInt, elapsed: Int) {
        
        if uid != self.uid{
            remoteUid = uid
            self.setTimer()
            self.stopRingingSound()
            
            if isCalling {
                self.updateLog(false, didAnswer: true)
            }
            
        }
        
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        
        self.stopTimer()
        self.lblTimer.text = CallStatus.decline.getString()
        
        self.playBusySound()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2){
            self.doEndCallPressed(self.endCallButton)
        }
        
        if isCalling {
            self.updateLog(true, didAnswer: false)
        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didAudioRouteChanged routing: AgoraAudioOutputRouting) {
        print("didAudioRouteChanged : \(routing.rawValue)")
//        if audioOutputRouting == nil && routing == .earpiece {
//            rtcEngine?.setEnableSpeakerphone(true)
//        }
//        else {
//            audioOutputRouting = routing;
//        }
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("error occured in RTC engine: \(errorCode.rawValue)" )
    }
    
    func rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int) {
        print("did join :")
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionMode(_ input: AVAudioSession.Mode) -> String {
	return input.rawValue
}
