//
//  RecordAudioReplyMediaControl.swift
//  Vobble
//
//  Created by Molham on 7/11/19.
//
//

import AVFoundation
import UIKit
import Flurry_iOS_SDK
import SDRecordButton


class RecordAudioReplyMediaControl : AbstractController {
    
    @IBOutlet weak var backButton:UIButton!
    @IBOutlet weak var startButton: VobbleButton!
    
    //record
    @IBOutlet var recordButton : SDRecordButton!
    @IBOutlet var vPulseSource : UIView!
    @IBOutlet var lblRecording : UILabel!
    @IBOutlet var lblTimerRecording : UILabel!
    @IBOutlet var recordButtonContainer : UIView!
    @IBOutlet var btnPlayAudio : UIButton!
    @IBOutlet var btnRetakeAudio : UIButton!
    
    // throw audio variables
    var isThrow: Bool = true
    var shore: Shore?
    var topicId: String?
    var parentVC: UIViewController?
    var selectedShoreIndex: Int = -1
    
    //
    var showRecordComponent: Bool = false
    var isSubmit: Bool = false
    var isRecording: Bool = false
    var audioUrl: URL? = nil
    var soundRecorder : AVAudioRecorder!
    var audioPlayer : AVAudioPlayer?
    var isAnimating: Bool = false
    var timeOut: Float = 0.0
    var recordTimer: Timer?
    let recordSettings = [AVSampleRateKey : NSNumber(value: Float(44100.0)),
                          AVFormatIDKey : NSNumber(value: Int32(kAudioFormatMPEG4AAC)),
                          AVNumberOfChannelsKey : NSNumber(value: 2 as Int32),
                          AVEncoderAudioQualityKey : NSNumber(value: Int32(AVAudioQuality.medium.rawValue) as Int32)]
    
    /// Record beep
    var beepSound = NSURL(fileURLWithPath: Bundle.main.path(forResource: "audio_msg_beeb", ofType: "mp3")!)
    var beepPlayer : AVAudioPlayer!
    
    var isVOverlayApplyGradient:Bool = false
    
    var pulseArray = [CAShapeLayer]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false);
        
        // itro animation
        backButton.animateIn(mode: .animateInFromTop, delay: 0.2)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        self.avPlayer.pause()
    }
    
    override func viewDidLayoutSubviews() {
        if !isVOverlayApplyGradient {
            
            self.recordButtonContainer.applyGradient(colours: [AppColors.blueXDark, AppColors.blueXLight], direction: .diagonal)
            
            setupRecorder()
            isVOverlayApplyGradient = true
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    func initViews() {
        
        self.startButton.setTitle("AUDIO_REPLY_START".localized, for: .normal)
        
        // record audio view
        lblRecording.font = AppFonts.bigBold
        lblRecording.text = "AUDIO_REPLY_TITLE".localized
        lblTimerRecording.font = AppFonts.bigBold
        lblTimerRecording.text = "AUDIO_REPLY_RECORD".localized
        
        self.btnPlayAudio.isHidden = true
        self.btnRetakeAudio.isHidden = true
        
        self.backButton.tintColor = UIColor.white
        
//        let longGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(self.didPressRecordAudio(_:)))
//        longGestureRecognizer.minimumPressDuration = 0.3
//        self.startButton.addGestureRecognizer(longGestureRecognizer)
        
        self.startButton.bringToFront()
        
        setupRecorder()
        
        vPulseSource.layer.cornerRadius = vPulseSource.frame.size.width/2.0
        createPulse()
        
    }
    
    func playerItemDidReachEnd(notification: NSNotification) {
//        self.avPlayer.seek(to: kCMTimeZero)
    }
    
    func playBeepSound () {
        // play beep sound
        do {
            // Prepare beep player
            self.beepPlayer = try AVAudioPlayer(contentsOf: beepSound as URL)
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: AVAudioSession.Mode.default)
            try AVAudioSession.sharedInstance().setActive(true)
            self.beepPlayer.prepareToPlay()
            self.beepPlayer.play()
            
        }catch {}
    }
    
    @IBAction func dissmiss() {
        
        if self.isRecording {
            self.recordTimer?.invalidate()
        }
        
        self.popOrDismissViewControllerAnimated(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool{
        return true
    }

    // animations
    func createPulse() {
        
        for _ in 0...2 {
            let circularPath = UIBezierPath(arcCenter: .zero, radius: ((self.vPulseSource.superview?.frame.size.width )! )/2, startAngle: 0, endAngle: 2 * .pi , clockwise: true)
            let pulsatingLayer = CAShapeLayer()
            pulsatingLayer.path = circularPath.cgPath
            pulsatingLayer.lineWidth = 2.5
            pulsatingLayer.fillColor = UIColor.clear.cgColor
            pulsatingLayer.lineCap = CAShapeLayerLineCap.round
            pulsatingLayer.position = CGPoint(x: vPulseSource.frame.size.width / 2.0, y: vPulseSource.frame.size.width / 2.0)
            vPulseSource.layer.addSublayer(pulsatingLayer)
            pulseArray.append(pulsatingLayer)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.animatePulsatingLayerAt(index: 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                self.animatePulsatingLayerAt(index: 1)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.animatePulsatingLayerAt(index: 2)
                })
            })
        })
    }

    func animatePulsatingLayerAt(index:Int) {
        
        //Giving color to the layer
        pulseArray[index].strokeColor = UIColor.darkGray.cgColor
        
        //Creating scale animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = 0.0
        scaleAnimation.toValue = 0.9
        
        //Creating opacity animation for the layer, from and to value should be in range of 0.0 to 1.0
        // 0.0 = minimum
        //1.0 = maximum
        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.fromValue = 0.9
        opacityAnimation.toValue = 0.0
        
        // Grouping both animations and giving animation duration, animation repat count
        let groupAnimation = CAAnimationGroup()
        groupAnimation.animations = [scaleAnimation, opacityAnimation]
        groupAnimation.duration = 2.3
        groupAnimation.repeatCount = .greatestFiniteMagnitude
        groupAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        //adding groupanimation to the layer
        pulseArray[index].add(groupAnimation, forKey: "groupanimation")
        
    }

    func submitReply(_ url: URL) {
        // throw bottle with audio record
        let urls: [URL] = [url]
        showActivityLoader(true)

        ApiManager.shared.uploadMedia(urls: urls, mediaType: .audio, completionBlock: { (files, errorMessage) in
            //
            if errorMessage == nil {
                
                let logEventParams = ["Shore": self.shore?.name_en ?? "", "mediaType" : "audio"];
                Flurry.logEvent(AppConfig.throw_shore_selected, withParameters:logEventParams);
                
                let bottle = Bottle()
                bottle.attachment = files[0].fileUrl ?? " "
                bottle.thumb = ""
                bottle.ownerId = DataStore.shared.me?.objectId
                bottle.owner = DataStore.shared.me
                bottle.status = "pending"
                bottle.shoreId = self.shore?.shore_id
                bottle.topicId = self.topicId
                bottle.bottleType = "audio"
                
                ApiManager.shared.addBottle(bottle: bottle, completionBlock: { (success, error, bottle) in
                    
                    if error == nil {
                        self.showActivityLoader(false)

                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // delay 6 second
                            self.performSegue(withIdentifier: "unwindRecordMediaSegue", sender: self)
                            self.parentVC?.popOrDismissViewControllerAnimated(animated: true)
                        }
                    } else {
                        self.showActivityLoader(false)
                        
                        let alertController = UIAlertController(title: "", message: error?.type.errorMessage , preferredStyle: .alert)
                        let ok = UIAlertAction(title: "ok".localized, style: .default,  handler: nil)
                        alertController.addAction(ok)
                        self.present(alertController, animated: true, completion: nil)
                        //print(error?.type.errorMessage)
                    }
                })
            } else {
                self.showActivityLoader(false)
                print(errorMessage ?? "unknown error occured in upload bottle")

            }
        }, progressBlock:{ (progressPercent) in
            if let percent = progressPercent {

            }
        })
        
//        Flurry.logEvent(AppConfig.reply_shooted);
//        self.performSegue(withIdentifier: "unwindToFindBottleSegue", sender: self)
//        self.popOrDismissViewControllerAnimated(animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        if (segue.identifier == "unwindToFindBottleSegue") {
            
//            let nav = segue.destination as! UINavigationController
            let findBottleVC = segue.destination  as! FindBottleViewController
            findBottleVC.myAudioUrl = self.audioUrl
        }
    }
    
}

//TODO: make custom chat tool bar class
// MARK:- AVAudioRecorderDelegate
extension RecordAudioReplyMediaControl: AVAudioRecorderDelegate {
    
    func didTabRecordAudio(_ sender: UITapGestureRecognizer) {
        
        
        let alertController = UIAlertController(title: "", message: "RECORD_LONG_PRESS".localized, preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok".localized, style: .default,  handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: nil)
        
        self.playBeepSound()
    }
    
    @IBAction func didPressPlayAudio() {
        
        if let audioPlayer = audioPlayer, audioPlayer.isPlaying {
            self.audioPlayer?.stop()
            self.btnPlayAudio.setImage(UIImage(named: "ic_play"), for: .normal)
        }else {
            do {
                if let url = audioUrl {
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: AVAudioSession.Mode.default)
                    try AVAudioSession.sharedInstance().setActive(true)
                    
                    self.audioPlayer = try AVAudioPlayer(contentsOf: url )
                    self.audioPlayer?.prepareToPlay()
                    self.audioPlayer?.volume = 1
                    self.audioPlayer?.delegate = self
                    
                    
                    self.audioPlayer?.play()
                    self.btnPlayAudio.setImage(UIImage(named: "pause"), for: .normal)
                }
                
                
            }catch{
                self.btnPlayAudio.setImage(UIImage(named: "ic_play"), for: .normal)
            }
        }
        
    }
    
    @IBAction func didPressRetakeAudio() {
        self.lblTimerRecording.text = "AUDIO_REPLY_RECORD".localized
        self.startButton.setTitle("AUDIO_REPLY_START".localized, for: .normal)
        self.isSubmit = false
        self.audioUrl = nil
        
        showHideRecordComponents()
    }
    
    @IBAction func didPressRecordAudio() {
        if isSubmit {
            if let url = audioUrl {
                submitReply(url)
            }
        }else {
            if self.isRecording == false {
                
                self.startButton.setTitle("AUDIO_REPLY_STOP".localized, for: .normal)
                
                // pop animation for the button
                UIView.animateKeyframes(withDuration: 1.0, delay: 0.0, options: [.calculationModeLinear], animations: {
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5, animations: {
                        self.startButton.transform = CGAffineTransform.identity.scaledBy(x: 1.2, y: 1.2)
                    })
                    UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                        self.startButton.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                    })
                    
                }, completion: { [weak self] (finished) in
                    self?.startButton.layer.removeAllAnimations()
                })
                
                self.isRecording = true
                self.playBeepSound()
                
                //write the function for start recording the voice here
                
                self.recordButton.setProgress(0.0)
                
                // reset the timer
                self.recordTimer?.invalidate()
                self.recordTimer = nil;
                // run the timer
                self.recordTimer = Timer.scheduledTimer(timeInterval: 0.05,
                                                        target: self,
                                                        selector: #selector(self.tickRecorder(timer:)),
                                                        userInfo: nil,
                                                        repeats: true)
                
                // run the timer
                let runner: RunLoop = RunLoop.current
                runner.add(self.recordTimer!, forMode: RunLoop.Mode.default)
                
                var recordingValid = true
                // we clear sound recorder after every recording session
                // so make sure we have a valid one before recording
                if  self.soundRecorder == nil {
                    do {
                        // todo: handle recording permission not granted
                        let audioSession:AVAudioSession = AVAudioSession.sharedInstance()
                        try audioSession.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playAndRecord)), mode: AVAudioSession.Mode.default)
                        try audioSession.setActive(true)
                        
                        try self.soundRecorder = AVAudioRecorder(url: self.directoryURL()!, settings: self.recordSettings)
                        recordingValid = true
                    } catch let error {
                        let errstr = "Error Recording \(error)"
                        print(errstr)
                        let alertController = UIAlertController(title: "", message: errstr, preferredStyle: .alert)
                        let ok = UIAlertAction(title: "ok".localized, style: .default,  handler: nil)
                        alertController.addAction(ok)
                        self.present(alertController, animated: true, completion: nil)
                        recordingValid = false
                    }
                }
                if recordingValid {
                    self.soundRecorder.prepareToRecord()
                    self.soundRecorder.delegate = self
                    self.soundRecorder.record()
                }
                
            } else {
                recordTimer?.invalidate()
                stopRecorderTimer()
                recordButton.setProgress(0.0)
                self.playBeepSound()
                self.isRecording = false
                
                self.startButton.setTitle("AUDIO_REPLY_START".localized, for: .normal)
                
                // pop animation for the button
                UIView.animateKeyframes(withDuration: 1.0, delay: 0.0, options: [.calculationModeLinear], animations: {
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5, animations: {
                        self.startButton.transform = CGAffineTransform.identity.scaledBy(x: 0.8, y: 0.8)
                    })
                    UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                        self.startButton.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                    })
                }, completion: { [weak self] (finished) in
                    self?.startButton.layer.removeAllAnimations()
                })
                
            }
        }
        
    }
    
    func setupRecorder(){
        
        let audioSession:AVAudioSession = AVAudioSession.sharedInstance()
        
        //ask for permission
        if (audioSession.responds(to: #selector(AVAudioSession.requestRecordPermission(_:)))) {
            AVAudioSession.sharedInstance().requestRecordPermission({ [weak self] (granted: Bool)-> Void in
                if granted {
                    //set category and activate recorder session
                    do {
                        try audioSession.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playAndRecord)), mode: AVAudioSession.Mode.default)
                        try audioSession.setActive(true)
                        //                        if let selfRef = self {
                        //                            try selfRef.soundRecorder = AVAudioRecorder(url: selfRef.directoryURL()!, settings: selfRef.recordSettings)
                        //                        }
                        //                        self?.soundRecorder.prepareToRecord()
                    } catch {
                        print("Error Recording");
                    }
                }
            })
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            let alertController = UIAlertController(title: "", message: "RECORD_FAILED_ERROR".localized, preferredStyle: .alert)
            let ok = UIAlertAction(title: "ok".localized, style: .default,  handler: nil)
            alertController.addAction(ok)
            self.present(alertController, animated: true, completion: nil)
        } else {
            showHideRecordComponents()
            self.startButton.setTitle("SUBMIT".localized, for: .normal)
            self.isSubmit = true
            self.lblTimerRecording.text = ""
        }
        soundRecorder = nil
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        
        let alertController = UIAlertController(title: "Audio recording error", message: error?.localizedDescription, preferredStyle: .alert)
        let ok = UIAlertAction(title: "ok".localized, style: .default,  handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func directoryURL() -> URL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as URL
        let soundURL = documentDirectory.appendingPathComponent("reqsound.m4a")
        audioUrl = soundURL
        return soundURL
    }
    
    // Stop play image
    @objc func tickRecorder(timer:Timer) {
        // count down 0
        if (timeOut >= MAX_VIDEO_LENGTH){
            // stop image timer
            recordTimer?.invalidate()
            recordTimer = nil;
            stopRecorderTimer()
        } else {
            timeOut += 0.05;
            self.lblTimerRecording.text = String(format: "%02d", Int(timeOut))
            self.recordButton.setProgress(CGFloat(timeOut/MAX_VIDEO_LENGTH))
        }
    }
    
    func stopRecorderTimer(){
        // stop recording
        self.recordButton.setProgress(0)
        timeOut = 0.0
        if soundRecorder != nil {
            soundRecorder.stop()
        }
    }
    
    func showHideRecordComponents(){
        // show
        
        if !self.showRecordComponent {
            self.btnPlayAudio.isHidden = self.showRecordComponent
            self.btnRetakeAudio.isHidden = self.showRecordComponent
            
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                self.btnPlayAudio.animateIn(mode: .animateInFromBottom  , delay: 0.3)
                self.btnRetakeAudio.animateIn(mode: .animateInFromBottom  , delay: 0.4)
                self.startButton.animateIn(mode: .animateOutToBottom, delay: 0.4)
            }, completion: {_ in
                
                UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseInOut, animations: {
                    
                    self.startButton.animateIn(mode: .animateOutToBottom, delay: 0)
                }, completion: {_ in
                    
                    UIView.animate(withDuration: 0.5, delay: 0.2, options: .curveEaseInOut, animations: {
                        
                        self.startButton.animateIn(mode: .animateInFromBottom, delay: 0.2)
                    }, completion: nil)
                })
            })

        }else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
                self.btnPlayAudio.animateIn(mode: .animateOutToBottom  , delay: 0.3)
                self.btnRetakeAudio.animateIn(mode: .animateOutToBottom  , delay: 0.4)
                
            }, completion: { _ in
                self.btnPlayAudio.isHidden = self.showRecordComponent
                self.btnRetakeAudio.isHidden = self.showRecordComponent
            })
        }
        
        self.showRecordComponent = !self.showRecordComponent
    }
}

// MARK:- Audio player delegate
extension RecordAudioReplyMediaControl: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.btnPlayAudio.setImage(UIImage(named: "ic_play"), for: .normal)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
