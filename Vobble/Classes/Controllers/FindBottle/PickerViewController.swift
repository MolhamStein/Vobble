//
//  PickerViewController.swift
//  Vobble
//
//  Created by Bayan on 3/4/18.
//  Copyright © 2018 Brain-Socket. All rights reserved.
//

import Foundation

class PickerViewController: AbstractController {
        
        
    @IBOutlet var vp: VideoPlayerView!
    
    let videoPickerController = UIImagePickerController()
    var videoURL: NSURL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
    }
    
    @IBAction func browseVideoBtn(_ sender: Any) {
        
        videoPickerController.sourceType = .photoLibrary
        videoPickerController.delegate = self
        videoPickerController.mediaTypes = ["public.movie"]
        
        present(videoPickerController, animated: true, completion: nil)

    }
    
}

extension PickerViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        videoURL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? NSURL
        videoPickerController.dismiss(animated: true, completion: nil)
        if let url = videoURL?.absoluteString {
//            vp.preparePlayer(videoURL: url)
        }
        
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
