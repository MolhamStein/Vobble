//
//  JSQCustomMessage.swift
//  Vobble
//
//  Created by Abd Hayek on 4/1/20.
//  Copyright © 2020 Brain-Socket. All rights reserved.
//

import Foundation
import JSQMessagesViewController

class JSQCustomMessage: JSQMessage {
    
    var message: Message = Message()
    
    init(message: Message) {
        super.init(senderId: message.senderId ?? "", senderDisplayName: "", date: Date(), text: message.text ?? "")
        self.message = message
        
    }
    
  
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
