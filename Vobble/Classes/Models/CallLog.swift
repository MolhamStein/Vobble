//
//  CallLog.swift
//  Vobble
//
//  Created by Abd Hayek on 1/14/20.
//  Copyright © 2020 Brain-Socket. All rights reserved.
//

import Foundation
import SwiftyJSON

enum CallStatus: String {
    case connecting = "connecting"
    case ringing = "ringing"
    case answering = "answering"
    case busy = "busy"
    case decline = "decline"
    case missedCall = "noAnswer"
    case notReceivingCalls = "notReceivingCalls"
    
    func getString() -> String {
        switch self {
        
        case .connecting:
            return "Connecting.."
        case .ringing:
            return "Ringing.."
        case .busy:
            return "In another call"
        case .decline:
            return "End call"
        case .missedCall:
            return "No Answer"
        case .answering:
            return "In Call"
        case .notReceivingCalls:
            return "User not receiving calls"
        }
    }
}

class CallLog: BaseModel {
    
    // MARK: Keys
    private let kId: String = "id"
    private let kConversationId: String = "conversationId"
    private let kStartAt: String = "startAt"
    private let kEndAt: String = "endAt"
    private let kIsFinish: String = "isFinish"
    private let kStaus: String = "status"
    private let kCreatedAt: String = "createdAt"
    private let kRelatedUserId: String = "relatedUserId"
    private let kOwnerId: String = "ownerId"
    private let kType: String = "type"
    
    // MARK: Properties
    public var logId: String?
    public var conversationId: String?
    public var startAt: String?
    public var endAt: String?
    public var isFinish: Bool?
    public var status: String?
    public var createdAt: String?
    public var relatedUserId: String?
    public var ownerId: String?
    public var type: String?
    
    // MARK: Initializers
    override init() {
        super.init()
    }
    
    required init(json: JSON) {
        super.init(json: json)
        
        if let value = json[kId].string {
            logId = value
        }
        
        if let value = json[kConversationId].string {
            conversationId = value
        }
        
        if let value = json[kStartAt].string {
            startAt = value
        }
        
        if let value = json[kEndAt].string {
            endAt = value
        }
        
        if let value = json[kIsFinish].bool {
            isFinish = value
        }
        
        if let value = json[kStaus].string {
            status = value
        }
        
        if let value = json[kCreatedAt].string {
            createdAt = value
        }
        
        if let value = json[kRelatedUserId].string {
            relatedUserId = value
        }
        
        if let value = json[kOwnerId].string {
            ownerId = value
        }
        
        if let value = json[kType].string {
            type = value
        }
        
        
        
    }
    
    override func dictionaryRepresentation() -> [String : Any] {
        var dictionary = super.dictionaryRepresentation()
        
        dictionary[kId] = logId
        dictionary[kCreatedAt] = createdAt
        dictionary[kConversationId] = conversationId
        dictionary[kOwnerId] = ownerId
        dictionary[kStaus] = status
        dictionary[kStartAt] = startAt
        dictionary[kEndAt] = endAt
        dictionary[kRelatedUserId] = relatedUserId
        dictionary[kIsFinish] = isFinish
        dictionary[kType] = type
        
        
        return dictionary
    }
}
