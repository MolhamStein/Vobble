//
//  ThrownBottleCollectionViewCell.swift
//  Vobble
//
//  Created by Abd hayek on 10/22/18.
//  Copyright © 2018 Brain-Socket. All rights reserved.
//

import UIKit

class ThrownBottleCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var bottleReplies: UILabel!
    @IBOutlet weak var bottleViews: UILabel!
    @IBOutlet weak var bottleShore: UILabel!
    @IBOutlet weak var bottleThumbnail: UIImageView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewsCountView: UIView!
    @IBOutlet weak var repliesCountView: UIView!
    
    fileprivate var bottle: Bottle?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bottleViews.font = AppFonts.small
        bottleReplies.font = AppFonts.small
        bottleShore.font = AppFonts.bigBold
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layoutIfNeeded()
        self.viewsCountView.layoutIfNeeded()
        self.repliesCountView.layoutIfNeeded()
        
        self.viewsCountView.removeGradientLayer()
        self.repliesCountView.removeGradientLayer()
        
        self.viewsCountView.applyGradient(colours: [AppColors.grayXDark, AppColors.grayXLight], direction: .diagonal)
        self.repliesCountView.applyGradient(colours: [AppColors.blueXDark, AppColors.blueXLight], direction: .diagonal)
    }
    
    func configCell(bottle: Bottle) {
        self.bottle = bottle
        
        bottleReplies.text = String.init(format: "MY_THROWN_BOTTLES_REPLIES_COUNT".localized, bottle.repliesUserCount ?? "0")
        bottleViews.text = String(bottle.bottleViewCount ?? 0)
        bottleShore.text = bottle.shore?.name_en
        
        
        if bottle.bottleType == "audio" {
            self.bottleThumbnail.contentMode = .center
            self.bottleThumbnail.image = #imageLiteral(resourceName: "ic_record_audio_big")
        }else {
            self.bottleThumbnail.contentMode = .scaleAspectFill
            if let iconUrl = bottle.thumb {
                bottleThumbnail.sd_setImage(with: URL(string: iconUrl))
            }
        }
        
        if bottle.viewStatus == "deactive" {
            self.alpha = 0.5
        }else {
            self.alpha = 1
        }
        
    }
    
}
