//
//  VideoPlayerView.swift
//  Vobble
//
//  Created by Bayan on 3/4/18.
//  Copyright © 2018 Brain-Socket. All rights reserved.
//
import Foundation
import UIKit
import CoreMedia
import AVFoundation
import TransitionButton
import KDEAudioPlayer
import SwiftyWave

@objc protocol MediaPlayerDelegate {
    @objc optional func didCompleteMedia()
    @objc optional func didSeenMedia()
    @objc optional func shakeVideoMedia()
}

class VideoPlayerLayer: AbstractNibView {
    
    @IBOutlet weak var wavesView: SwiftyWaveView!
    @IBOutlet weak var recordView: UIView!
    @IBOutlet weak var audioView: UIView!
    @IBOutlet weak var imgLeaf1: UIImageView!
    @IBOutlet weak var imgLeaf2: UIImageView!
    @IBOutlet weak var imgLeaf3: UIImageView!
    @IBOutlet weak var imgSeat: UIImageView!
    @IBOutlet weak var imgUserPhoto: UIImageView!
    
    
    fileprivate var playerLayer: AVPlayerLayer?
    fileprivate var player: AVPlayer?
    fileprivate var audioPlayer: AudioPlayer?
    fileprivate var asset: AVAsset?
    fileprivate var playerItem: AVPlayerItem?
    fileprivate var url: URL?
    fileprivate var playButton: TransitionButton?
    fileprivate var videoDelegate: MediaPlayerDelegate?
    fileprivate var tolerance: CMTime = CMTimeMakeWithSeconds(0.001, preferredTimescale: 1000)
    fileprivate var seekTime: CMTime!
    fileprivate var audioItem: AudioItem?
    fileprivate var pulseArray = [CAShapeLayer]()
    fileprivate var safeAudioPlayerPointer: UnsafeMutableRawPointer?
    fileprivate var safeAudioItemPointer: UnsafeMutableRawPointer?
    fileprivate var statusObserver: NSKeyValueObservation?
    fileprivate var timeControlStatusObserver: NSKeyValueObservation?
    
    public var isRecording: Bool = false
    public var isAudio: Bool = false
    public var slideBar: UISlider?
    public var isAutoPlay: Bool = false
    public var index: Int?
    public var userImage: String? = nil {
        didSet {
            if let imgUrl = userImage, imgUrl.isValidLink() {
                imgUserPhoto.sd_setShowActivityIndicatorView(true)
                imgUserPhoto.sd_setIndicatorStyle(.gray)
                imgUserPhoto.sd_setImage(with: URL(string: imgUrl))
            }else {
                imgUserPhoto.image = UIImage(named:"user_placeholder")
            }
        }
    }
    
    func configure(url: String?, isAutoPlay: Bool = false, customButton: TransitionButton, videoDelegate: MediaPlayerDelegate?, index: Int?, slideBar: UISlider, isAudio: Bool = false) {
        
        self.isAudio = isAudio
        
        if let url = URL(string: url ?? "") {
            
            if isAudio {
                self.isAutoPlay = isAutoPlay
                self.videoDelegate = videoDelegate
                self.url = url
                self.playButton = customButton
                self.index = index
                self.slideBar = slideBar
                self.isAudio = true
                self.recordView.isHidden = false
                self.audioView.isHidden = false
                self.wavesView.isHidden = false
                
                
//                self.imgLeaf1.transform = CGAffineTransform.identity.translatedBy(x: self.imgLeaf1.frame.width + 150, y: 0)
//                self.imgLeaf2.transform = CGAffineTransform.identity.translatedBy(x: self.imgLeaf2.frame.width + 150, y: 0)
//                self.imgLeaf3.transform = CGAffineTransform.identity.translatedBy(x: -(self.imgLeaf3.frame.width + 150), y: 0)
//
//                self.imgSeat.rotate(degrees: -30)
                
                
                self.audioPlayer = AudioPlayer()
                self.audioPlayer?.delegate = self
                
                if let item = AudioItem(highQualitySoundURL: url) {
                    self.audioItem = item
                    audioPlayer?.play(item: item)
                }
                
                UIView.animateKeyframes(withDuration: 1.0, delay: 0.0, options: [.calculationModeLinear, .repeat], animations: {
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5, animations: {
                        self.recordView.transform = CGAffineTransform.identity.scaledBy(x: 1.2, y: 1.2)
                    })
                    UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5, animations: {
                        self.recordView.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
                    })
                    
                }, completion: { [weak self] (finished) in
                    //self?.recordView.layer.removeAllAnimations()
                })

                
            }else {
                
                self.isAutoPlay = isAutoPlay
                self.url = url
                self.playButton = customButton
                self.videoDelegate = videoDelegate
                self.index = index
                self.slideBar = slideBar
                self.isAudio = false
                self.recordView.isHidden = true
                self.audioView.isHidden = true
                self.wavesView.isHidden = true
                
                asset = AVAsset(url: url)
                playerItem = AVPlayerItem(asset: asset!)
                player = AVPlayer(playerItem: playerItem)
                playerLayer = AVPlayerLayer(player: player)
                
                playerLayer?.frame = bounds
                playerLayer?.videoGravity = AVLayerVideoGravity.resize
                
                if let playerLayer = self.playerLayer {
                    layer.addSublayer(playerLayer)
                }
                
                addVideoObserversForFirstInit()
                
                setupTapGesture()
                
                // Notify when the video is played to the end
                NotificationCenter.default.addObserver(self, selector: #selector(reachTheEndOfTheVideo(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
            }
            
            
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.audioView.layoutIfNeeded()
        self.audioView.removeGradientLayer()
        self.audioView.applyGradient(colours: [UIColor(red: 185, green: 236, blue: 255), UIColor(red: 194, green: 227, blue: 246)], direction: .diagonal)
    }
    
    func addVideoObserversForFirstInit(){
        // This observer for seek bar
        player?.addPeriodicTimeObserver(forInterval: CMTime(seconds: 0.01, preferredTimescale: 1000), queue: DispatchQueue.main) {[weak self] (progressTime) in
            if let duration = self?.player?.currentItem?.duration {
                
                let durationSeconds = CMTimeGetSeconds(duration)
                let seconds = CMTimeGetSeconds(progressTime)
                let progress = Float(seconds/durationSeconds)
                
                DispatchQueue.main.async {
                    self?.slideBar?.value = progress
                    if progress >= 1.0 {
                        self?.slideBar?.value = 0.0
                    }

                    if Int(seconds) == 3 {
                        self?.videoDelegate?.didCompleteMedia!()
                    }

                }
            }
        }
        
        self.statusObserver = playerItem?.observe(\.status, options: .new, changeHandler: {(_, change) in
            
            let status: AVPlayerItem.Status
            
            if let playerItemStatus = self.playerItem?.status  {
                status = playerItemStatus
            } else {
                status = .unknown
            }
            
            // Switch over status value
            switch status {
            case .readyToPlay:
                if self.isAutoPlay {
                    if self.isRecording {
                        self.pause()
                    }else {
                        self.play(true)
                    }
                    
                }else{
                    self.pause()
                }
            case .failed:
                return
            case .unknown:
                return
            }
        })
        //playerItem?.addObserver(self, forKeyPath: "status", options: [.old, .new], context: safeAudioItemPointer)
        if #available(iOS 10.0, *) {
            timeControlStatusObserver = player?.observe(\.timeControlStatus, options: [.old, .new], changeHandler: {(_, _) in
                
                if let newStatus = self.player?.timeControlStatus {
                    
                    DispatchQueue.main.async {[weak self] in
                        
                        if self?.isAutoPlay ?? false {
                            if newStatus == .playing {
                                
                                self?.playButton?.stopAnimation(animationStyle: .normal, revertAfterDelay: 0 , completion: {
                                    self?.playButton?.setImage(UIImage(named: "pause"), for: .normal)
                                })
                                
                                if self?.isRecording ?? false{
                                    self?.pause()
                                }
                                
                            }else if newStatus == .paused {
                                //self?.playButton?.setImage(UIImage(named: "ic_play"), for: .normal)
                                self?.playButton?.stopAnimation(animationStyle: .normal, revertAfterDelay: 0 , completion: {
                                    self?.playButton?.setImage(UIImage(named: "ic_play"), for: .normal)
                                })
                            }else {
                                self?.playButton?.setImage(UIImage(named: "pause"), for: .normal)
                                self?.playButton?.startAnimation()
                            }
                        }
                    }
                    
                }
            })
        } else {
            player?.addObserver(self, forKeyPath: "timeControlStatus", options: [.old, .new], context: safeAudioPlayerPointer)
        }
        
        
    }
    
    func removeVideoObservers() {
        if let playerItem = self.playerItem, let player = self.player {
            playerItem.removeObserver(self, forKeyPath: "status", context: nil)
            player.removeObserver(self, forKeyPath: "timeControlStatus", context: nil)
            
            pause()
        }
    }
    
    func setupTapGesture(){
        let tapGestureRecognizer: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapGestureRecognizer(_:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func reachTheEndOfTheVideo(_ notification: Notification) {
        stop()
        videoDelegate?.didCompleteMedia!()
    }
    
    func createPulse() {
        // stops all layers animation before starting a new one
        self.stopWavesAnimations()
        for _ in 0...2 {
            let circularPath = UIBezierPath(arcCenter: .zero, radius: ((self.superview?.frame.size.width )! )/2, startAngle: 0, endAngle: 2 * .pi , clockwise: true)
            let pulsatingLayer = CAShapeLayer()
            pulsatingLayer.path = circularPath.cgPath
            pulsatingLayer.lineWidth = 2.5
            pulsatingLayer.fillColor = UIColor.clear.cgColor
            pulsatingLayer.lineCap = CAShapeLayerLineCap.round
            pulsatingLayer.position = CGPoint(x: self.frame.size.width / 2.0, y: self.frame.size.height / 2.0)
            self.layer.addSublayer(pulsatingLayer)
            pulseArray.append(pulsatingLayer)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.animatePulsatingLayerAt(index: 0)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4, execute: {
                self.animatePulsatingLayerAt(index: 1)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    self.animatePulsatingLayerAt(index: 2)
                })
            })
        })
    }
    
    func animatePulsatingLayerAt(index:Int) {
        if pulseArray.count > 0 {
            //Giving color to the layer
            pulseArray[index].strokeColor = #colorLiteral(red: 0.2986887991, green: 0.6970970631, blue: 1, alpha: 1)
            
            //Creating scale animation for the layer, from and to value should be in range of 0.0 to 1.0
            // 0.0 = minimum
            //1.0 = maximum
            let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
            scaleAnimation.fromValue = 0.0
            scaleAnimation.toValue = 0.9
            
            //Creating opacity animation for the layer, from and to value should be in range of 0.0 to 1.0
            // 0.0 = minimum
            //1.0 = maximum
            let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
            opacityAnimation.fromValue = 0.9
            opacityAnimation.toValue = 0.0
            
            // Grouping both animations and giving animation duration, animation repat count
            let groupAnimation = CAAnimationGroup()
            groupAnimation.animations = [scaleAnimation, opacityAnimation]
            groupAnimation.duration = 2.3
            groupAnimation.repeatCount = .greatestFiniteMagnitude
            groupAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            //adding groupanimation to the layer
            pulseArray[index].add(groupAnimation, forKey: "groupanimation")
        }
 
    }
    
    func stopWavesAnimations(){
        if pulseArray.count == 3 {
            for i in 0...2 {
                pulseArray[i].removeAnimation(forKey: "groupanimation")
                pulseArray[i].removeFromSuperlayer()
            }

            pulseArray = []
        }

    }
    
    func userInteractions(_ stop: Bool){
        self.isUserInteractionEnabled = stop
        self.playButton?.isUserInteractionEnabled = stop
    }
    
    func setupAudioInterface(){

        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.imgLeaf1.transform = CGAffineTransform.identity
            self.imgLeaf1.rotate(degrees: 30)

        }, completion: { _ in
            //self.imgLeaf1.rotateWithAnimation(angle: -30°)
            self.imgLeaf1.setAnchorPoint(anchorPoint: CGPoint(x: 0.250 , y: 0))
            UIView.animate(withDuration: 1.5, delay: 0, options: [.repeat, .autoreverse], animations: {
                self.imgLeaf1.rotate(degrees: 34)
            }, completion: nil)

        })

        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.imgLeaf2.transform = CGAffineTransform.identity
            self.imgLeaf2.rotate(degrees: -30)

        }, completion: { _ in
            //self.imgLeaf1.rotateWithAnimation(angle: -30°)
            self.imgLeaf2.setAnchorPoint(anchorPoint: CGPoint(x: 1 , y: 0))
            UIView.animate(withDuration: 1.5, delay: 1.2, options: [.repeat, .autoreverse], animations: {
                self.imgLeaf2.rotate(degrees: -33)
            }, completion: nil)

        })

        UIView.animate(withDuration: 1, delay: 0.7, usingSpringWithDamping: 0.8, initialSpringVelocity: 5, options: .curveEaseInOut, animations: {
            self.imgLeaf3.transform = CGAffineTransform.identity
            self.imgLeaf3.rotate(degrees: -60)

        }, completion: { _ in
            //self.imgLeaf1.rotateWithAnimation(angle: -30°)
            self.imgLeaf3.setAnchorPoint(anchorPoint: CGPoint(x: 0.250 , y: 0))
            UIView.animate(withDuration: 1.5, delay: 0, options: [.repeat, .autoreverse], animations: {
                self.imgLeaf3.rotate(degrees: -65)
            }, completion: nil)

        })
        
    }
    
    @objc
    func handleTapGestureRecognizer(_ gestureRecognizer: UITapGestureRecognizer) {
        playButtonPressed()
        
    }
    
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "timeControlStatus", let change = change, let newValue = change[NSKeyValueChangeKey.newKey] as? Int, let oldValue = change[NSKeyValueChangeKey.oldKey] as? Int {
            
            if #available(iOS 10.0, *) {
                let oldStatus = AVPlayer.TimeControlStatus(rawValue: oldValue)
                let newStatus = AVPlayer.TimeControlStatus(rawValue: newValue)
                
                if newStatus != oldStatus {
                    DispatchQueue.main.async {[weak self] in
                        
                        if self?.isAutoPlay ?? false {
                            if newStatus == .playing {
                                
                                self?.playButton?.stopAnimation(animationStyle: .normal, revertAfterDelay: 0 , completion: {
                                    self?.playButton?.setImage(UIImage(named: "pause"), for: .normal)
                                })
                                
                                if self?.isRecording ?? false{
                                    self?.pause()
                                }
                                
                            }else if newStatus == .paused {
                                //self?.playButton?.setImage(UIImage(named: "ic_play"), for: .normal)
                                self?.playButton?.stopAnimation(animationStyle: .normal, revertAfterDelay: 0 , completion: {
                                    self?.playButton?.setImage(UIImage(named: "ic_play"), for: .normal)
                                })
                            }else {
                                self?.playButton?.setImage(UIImage(named: "pause"), for: .normal)
                                self?.playButton?.startAnimation()
                            }
                        }
                    }
                }
                
            } else {
                // Fallback on earlier versions
            }
            
        }else if keyPath == "status" {
            let status: AVPlayerItem.Status
            if let statusNumber = change?[.newKey] as? NSNumber {
                status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
            } else {
                status = .unknown
            }
            
            // Switch over status value
            switch status {
            case .readyToPlay:
                if self.isAutoPlay {
                    if self.isRecording {
                        self.pause()
                    }else {
                        self.play(true)
                    }
                    
                }else{
                    pause()
                }
            case .failed:
                return
            case .unknown:
                return
            }
        }
    }
    
}

// MARK:- Player Control Functions
extension VideoPlayerLayer {
    func play(_ isSeen: Bool = false) {
        if isAudio {
            if audioPlayer?.state == .stopped {
                audioPlayer?.play(item: audioItem!)
            }else {
                //self.createPulse()
                self.wavesView.start()
                // 
                self.playButton?.stopAnimation(animationStyle: .normal, revertAfterDelay: 0 , completion: {
                    self.playButton?.setImage(UIImage(named: "pause"), for: .normal)
                })

                audioPlayer?.resume()
            }
        }else {
            player?.play()
        }

//        if isSeen {
//            self.videoDelegate?.didSeenMedia!()
//        }
    }
    
    func pause() {
        if isAudio {
            audioPlayer?.pause()
        }else {
            player?.pause()
        }
        
    }
    
    func stop() {
        if isAudio {
            audioPlayer?.stop()
            audioPlayer?.seek(to: 0.0)
        }else {
            player?.pause()
            player?.seek(to: CMTime.zero)
        }
        
    }
    
    func seekToTime(_ sender: UISlider) {
        if isAudio {
            //self.pause()
            //self.seekTime = CMTimeMakeWithSeconds(Float64(Double(sender.value) * (Int(self.audioPlayer?.currentItemDuration ?? 0) * 60)), 1000)
            //self.audioPlayer?.seek(to: <#T##TimeInterval#>)
            //self.audioPlayer?.seek(to: TimeInterval(sender.value * 100))
            
        }else {
            self.pause()
            self.seekTime = CMTimeMakeWithSeconds(Float64(Double(sender.value) * (self.player?.currentItem?.duration.seconds ?? 0.0)), preferredTimescale: 1000)
            self.player?.seek(to: seekTime, toleranceBefore: self.tolerance, toleranceAfter: self.tolerance)
        }
        
    }
    
    func isPlaying() -> Bool {
        if isAudio {
            if let player = self.audioPlayer {
                if #available(iOS 10.0, *) {
                    if player.state == .playing {
                        return true
                    }
                } else {
                    // Fallback on earlier versions
                }
            }
        }else {
            if let player = self.player {
                if #available(iOS 10.0, *) {
                    if player.timeControlStatus == AVPlayer.TimeControlStatus.playing {
                        return true
                    }
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        
        return false
    }
    
    func isMediaAvailable() -> Bool{
        if let _ = self.url {
            return true
        }
        return false
    }
    
    func isVideoReady() -> Bool {
        if let playerItem = self.playerItem {
            if playerItem.status == AVPlayerItem.Status.readyToPlay {
                return true
            }
        }
        return false
    }
    
    func cancelBuffring() {
        if let playerItem = self.playerItem, let asset = self.asset, let player = self.player {
            
            asset.cancelLoading()
            playerItem.cancelPendingSeeks()
            
//            if playerItem.observationInfo != nil {
//                playerItem.removeObserver(self, forKeyPath: "status", context: self.safeAudioItemPointer)
//            }
            
            self.statusObserver?.invalidate()
            self.statusObserver = nil

            if let _ = self.safeAudioPlayerPointer {
                if player.observationInfo != nil {
                    player.removeObserver(self, forKeyPath: "timeControlStatus", context: self.safeAudioPlayerPointer)
                }
            }else {
                self.timeControlStatusObserver?.invalidate()
                self.timeControlStatusObserver = nil
            }

            player.cancelPendingPrerolls()
            player.replaceCurrentItem(with: nil)
            
            stop()
        }
        
        
        if let _ = self.audioPlayer {
            stop()
            self.audioItem = nil
            self.audioPlayer = nil
        }
    }

    func reloadVideoPrerolls(){
        if let player = player {
            player.preroll(atRate: 0, completionHandler: nil)
            play()
        }
    }
    
    func playButtonPressed() {
        if isPlaying() {
            pause()
        }else {
            play()
        }
    }
    
    func getAudioDuration() -> Double? {
        let time = NSInteger(self.audioPlayer?.currentItemDuration ?? 0)
        
        return Double(time % 60)
    }
}

// MARK:- Audio player delegate
extension VideoPlayerLayer: AudioPlayerDelegate {
    func audioPlayer(_ audioPlayer: AudioPlayer, willStartPlaying item: AudioItem) {
        if self.isAutoPlay {
            
            if self.isRecording {
                self.pause()
            }else {
                self.play(true)
                //self.createPulse()
                self.wavesView.start()
            }
            
        }else {
            self.pause()
        }
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didChangeStateFrom from: AudioPlayerState, to state: AudioPlayerState) {
        if self.isAutoPlay {
            if state == .playing {
                
                self.playButton?.stopAnimation(animationStyle: .normal, revertAfterDelay: 0 , completion: {
                    self.playButton?.setImage(UIImage(named: "pause"), for: .normal)
                })
                
                if self.isRecording {
                    self.pause()
                }
            }else if state == .paused {
                
                self.playButton?.stopAnimation(animationStyle: .normal, revertAfterDelay: 0 , completion: {
                    self.playButton?.setImage(UIImage(named: "ic_play"), for: .normal)
                })
                
                //self.stopWavesAnimations()
                self.wavesView.stop()
                
            }else if state == .stopped {
            
                self.playButton?.stopAnimation(animationStyle: .normal, revertAfterDelay: 0 , completion: {
                    self.playButton?.setImage(UIImage(named: "ic_play"), for: .normal)
                })
                
                //self.stopWavesAnimations()
                self.wavesView.stop()
            }else {
                self.playButton?.setImage(UIImage(named: "pause"), for: .normal)
                self.playButton?.startAnimation()
            }
        }
        
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didUpdateProgressionTo time: TimeInterval, percentageRead: Float) {
        
        if Int(time) == 3 {
            self.videoDelegate?.didCompleteMedia!()
        }
        
        self.slideBar?.value = percentageRead / 100

        if percentageRead == 100.0 {
            self.slideBar?.value = 0.0
            self.videoDelegate?.didCompleteMedia!()
        }
    }
    
    func audioPlayer(_ audioPlayer: AudioPlayer, didLoad range: TimeRange, for item: AudioItem) {
        
    }
}
