//
//  AppDelegate.swift
//  BrainSocket Code base
//
//  Created by Molham Mahmoud on 4/25/17.
//  Copyright © 2017 BrainSocket. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import FBSDKCoreKit
import TwitterKit
import Firebase
import Crashlytics
import Fabric
import OneSignal
import Flurry_iOS_SDK
import PushKit
import SwiftyJSON
import AgoraRtcKit
import Branch

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    // MARK: Properties
    var window: UIWindow?
    var voiceCallVC: VoiceCallViewController?

    // MARK: Application Cycle
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // for dark mode
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        } else {
            // Fallback on earlier versions
        }
        
        // Override point for customization after application launch.
        if (AppConfig.useCurrentLocation) {
            LocationHelper.shared.startUpdateLocation()
        }
        // set navigation style
        AppConfig.setNavigationStyle()
        // init social managers
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        TWTRTwitter.sharedInstance().start(withConsumerKey: AppConfig.twitterConsumerKey, consumerSecret: AppConfig.twitterConsumerSecret)
        FirebaseApp.configure()
        Fabric.sharedSDK().debug = !AppConfig.isProductionBuild
        
        // Branch setup
        // if you are using the TEST key
        //Branch.setUseTestBranchKey(true)
        // listener for Branch Deep Link data
        Branch.getInstance().initSession(launchOptions: launchOptions) { (params, error) in
            // do stuff with deep link data (nav to page, display content, etc)
            print(params as? [String: AnyObject] ?? {})
        }
        
        //FirebaseCrash
        //FirebaseConfiguration.sharedInstance().crashCollectionEnabled = AppConfig.isProductionBuild
        // init managers
//        DataStore.shared
//        FirebaseManager.shared
        
        // PushKit Registeration
        self.voipRegistration()
        
        // init notification
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload? = result?.notification.payload
            
//            print("Message = \(payload!.body)")
//            print("badge number = \(payload?.badge ?? 0)")
//            print("notification sound = \(payload?.sound ?? "None")")
            
            // This block of code is for Tiktok screen
            if let vc = UIApplication.visibleViewController() as? FindBottleViewController {
                vc.isGoingToSubViewController = true
            }
            
            if let additionalData = result!.notification.payload!.additionalData {
                
                //print("additionalData = \(additionalData)")
                if let actionSelected = payload?.actionButtons {
                    print("actionSelected = \(actionSelected)")
                }
                
                if let chatId = additionalData["chatId"] as? String {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        if DataStore.shared.isLoggedin {
                            ActionOpenChat.execute(chatId: chatId, conversation: nil)
                        }
                    }
                } else if let _ = additionalData["throwBottle"] as? String {
                    // broadcast
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        if DataStore.shared.isLoggedin {
                            NotificationCenter.default.post(name: Notification.Name("OpenThrowBottle"), object: nil)
                        }
                    }
                } else if let bottleId = additionalData["bottleId"] as? String {
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        if DataStore.shared.isLoggedin {
                            NotificationCenter.default.post(name: Notification.Name("OpenBottleById"), object: bottleId)
                        }
                    }
                    //                        self.window?.rootViewController = instantiatedGreenViewController
                    //                        self.window?.makeKeyAndVisible()
                }
                
                // DEEP LINK from action buttons
                if let actionID = result?.action.actionID {
                    
                    // For presenting a ViewController from push notification action button
//                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                    let instantiateRedViewController : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "RedViewControllerID") as UIViewController
//                    let instantiatedGreenViewController: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "GreenViewControllerID") as UIViewController
//                    self.window = UIWindow(frame: UIScreen.main.bounds)
                    
                    
                    print("actionID = \(actionID)")
                    
                }
            }
            
        }
        
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        
        // Replace 'YOUR_APP_ID' with your OneSignal App ID.
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: AppConfig.oneSingleID,
                                        handleNotificationAction: notificationOpenedBlock,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        
        // flurry
        Flurry.startSession("7B59N7DQJQKHKGVY9BJ8", with: FlurrySessionBuilder
            .init()
            .withCrashReporting(true)
            .withLogLevel(FlurryLogLevelCriticalOnly))
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        Branch.getInstance().application(app, open: url, options: options)
        return ApplicationDelegate.shared.application(app, open: url, options: options) ||
            TWTRTwitter.sharedInstance().application(app, open: url, options: options) ||
            GIDSignIn.sharedInstance().handle(url)
    }

    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        // handler for Universal Links
        Branch.getInstance().continue(userActivity)
        return true
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // handler for Push Notifications
        Branch.getInstance().handlePushNotification(userInfo)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        print("App will resign active")
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        DataStore.shared.versionChecked = false
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        print("App will enter foreground")
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        print("App did become active")
        if AppConfig.shouldDisplayCallUI {
            if let vc = self.voiceCallVC {
                UIApplication.visibleViewController()?.present(vc, animated: true, completion: nil)
                AppConfig.shouldDisplayCallUI = false
            }
            
        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        DataStore.shared.versionChecked = false
    }
    
}

// MARK:- PushKit Section
extension AppDelegate: PKPushRegistryDelegate {
    // Handle updated push credentials
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        // Register VoIP push token (a property of PKPushCredentials) with server
        let token = pushCredentials.token.map{ String(format: "%02.2hhx", $0) }.joined()
        DataStore.shared.pushKitToken = token
        
        if let _ = DataStore.shared.me {
            ApiManager.shared.updatePushToken(completionBlock: {_,_,_ in})
        }
        print("PushKit Token: \(token)")
        
    }
    
    // Handle incoming pushes
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {
        // Process the received push
        print("Recieving call")
        	
        if type == .voIP {
            // Extract the call information from the push notification payload
            let json = JSON(payload.dictionaryPayload as Any)
            if let _ = json["owner"].dictionary, let key = json["callId"].string {
                let user = AppUser(json: json["owner"])
                if #available(iOS 10.0, *) {
                    
                    FirebaseManager.shared.changeVoipStatus(status: AppConfig.isAnotherCall ? .busy : .ringing, key: key )
                    
                    if !AppConfig.isAnotherCall {
                        CallCenter.shared.channelKey = key
                        CallCenter.shared.showIncomingCall(of: user.userName ?? "")
                        
                        self.voiceCallVC = UIStoryboard.mainStoryboard.instantiateViewController(withIdentifier: VoiceCallViewController.className) as? VoiceCallViewController
                        self.voiceCallVC?.session = user.userName ?? ""
                        self.voiceCallVC?.user = user
                        self.voiceCallVC?.channelId = json["conversationId"].string ?? ""
                    }
                    
                    
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }

    // Register for VoIP notifications
    func voipRegistration() {
        let mainQueue = DispatchQueue.main
        // Create a push registry object
        let voipRegistry: PKPushRegistry = PKPushRegistry(queue: mainQueue)
        // Set the registry's delegate to self
        voipRegistry.delegate = self
        // Set the push type to VoIP
        voipRegistry.desiredPushTypes = [PKPushType.voIP]
    }

}
